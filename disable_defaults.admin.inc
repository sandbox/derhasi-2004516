<?php

/**
 * @file
 * Holds administrative overview for disable defaults.
 */

/**
 * Form callback for admin settings & overview.
 *
 * @todo Show export for settings.php
 */
function disable_defaults_admin_form($form, &$form_state) {

  $forced = disable_defaults_config_is_forced();

  if ($forced) {
    drupal_set_message(t('The configuration cannot be saved as the configuration is overwritten in settings.php.'), 'warning');
  }

  $hook_implementation_status = disable_defaults_hook_implementation_status();

  $form['hooks'] = array(
    '#type' => 'vertical_tabs',
    '#tree' => TRUE,
  );

  $header = array(
    'module' => t('Module'),
    'implementation' => t('Implementation'),
    'status' => t('Status'),
  );

  foreach ($hook_implementation_status as $hook => $module_status) {

    $form['hooks'][$hook] = array(
      '#type' => 'fieldset',
      '#title' => check_plain("hook_$hook()"),
      '#collapsible' => TRUE,
    );

    $form['hooks'][$hook]['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#empty' => t('No modules implement this hook.'),
      '#default_value' => array(),
      '#options' => array(),
    );

    foreach ($module_status as $module => $status) {
      $form['hooks'][$hook]['table']['#options'][$module] = array(
        'module' => check_plain($module),
        'implementation' => check_plain("{$module}_{$hook}()"),
        'status' => check_plain($status),
      );
      $form['hooks'][$hook]['table']['#default_value'][$module] = ($status == DISABLE_DEFAULTS_STATUS_ENABLED);
      if ($status == DISABLE_DEFAULTS_STATUS_MISSING) {
        $form[$hook]['table'][$module]['#disabled'] = TRUE;
      }
    }

    // Option to remove settings.
    $form['hooks'][$hook]['remove'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove settings for %hook.', array('%hook' => "hook_$hook()")),
      '#collapsible' => TRUE,
    );
  }

  // Option to add new hook.
  $form['add_hook'] = array(
    '#type' => 'textfield',
    '#title' => t('Add new hook'),
    '#default_value' => '',
    '#description' => t('Enter the name of the hook without "hook_" to be added to disable defaults.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#disabled' => $forced,
  );

  return $form;
}

/**
 * Validate disable_defaults_admin_form.
 */
function disable_defaults_admin_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['add_hook'])) {
    if (!preg_match('/\w+/', $form_state['values']['add_hook'])) {
      form_set_error('add_hook', t('The given hook name is not valid.'));
    }
  }
}

/**
 * Submission callback for disable_defaults_admin_form.
 */
function disable_defaults_admin_form_submit($form, &$form_state) {

  $config = array();

  foreach ($form_state['values']['hooks'] as $hook => $content) {
    // We skip vertical tabs state and
    if (!isset($content['table']) || $content['remove']) {
      continue;
    }
    $config[$hook] = array_keys(array_diff_key($content['table'], array_filter($content['table'])));
  }

  // Check if we will need to flush the cache after the variable is set.
  $old_config = disable_defaults_config();
  $flush_cache = ($old_config != $config);

  // We need not to flush cache, if a new hook is added, as those will all be
  // enabled by default.
  if (!empty($form_state['values']['add_hook'])) {
    $config[$form_state['values']['add_hook']] = array();
  }

  variable_set('disable_defaults', $config);

  // We need to flush the cache, so defaults can be removed, when the config
  // changed.
  if ($flush_cache) {
    drupal_flush_all_caches();
  }
}

