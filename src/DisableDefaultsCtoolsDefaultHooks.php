<?php

/**
 * Representation of default hooks registered via ctools.
 */
class DisableDefaultsCtoolsDefaultHooks {

  /**
   * @var DisableDefaultsCtoolsAPI[string]
   *   Holding all available hooks as keys and assigned API as value.
   */
  protected $hooks = array();

  /**
   * @var string[string]
   */
  protected $apis = array();

  /**
   * Constructor.
   */
  public function __construct() {
    $this->init();
  }

  /**
   * Initialises the object.
   */
  protected function init() {
    $this->hooks = array();
    $this->apis = array();

    $schemas = drupal_get_complete_schema();
    foreach ($schemas as $table => $schema) {
      if (isset($schema['export']) && isset($schema['export']['default hook'])) {
        $this->register($schema['export']['default hook'], DisableDefaultsCtoolsAPI::fromExport($schema['export']));
      }
    }
  }

  /**
   * Registers a given hook/api combination to the object.
   *
   * @param string $default_hook
   * @param \DisableDefaultsCtoolsAPI $api
   */
  protected function register($default_hook, DisableDefaultsCtoolsAPI $api) {
    $this->hooks[$default_hook] = $api;

    // There can be mutiple hooks per api.
    $apiString = (string) $api;
    if (!isset($this->apis[$apiString])) {
      $this->apis[$apiString] = array();
    }
    $this->apis[$apiString][] = $default_hook;
  }

  /**
   * Retrieve default hooks for the given api.
   *
   * @param \DisableDefaultsCtoolsAPI $api
   *
   * @return string[]
   */
  public function getDefaultHooks(DisableDefaultsCtoolsAPI $api) {
    $apiString = (string) $api;
    if (isset($this->apis[$apiString])) {
      return $this->apis[$apiString];
    }

    return array();
  }

  /**
   * Retrieve API for the given default hook.
   *
   * @param string $default_hook
   *
   * @return DisableDefaultsCtoolsAPI
   */
  public function getApi($default_hook) {
    if (isset($this->hooks[$default_hook])) {
      return $this->hooks[$default_hook];
    }
  }

  /**
   * Static creator for a singleton.
   *
   * @return DisableDefaultsCtoolsDefaultHooks
   */
  public static function service() {
    // @todo: cache that
    static $instance;

    if (!isset($instance)) {
      $instance = new static();
    }
    return $instance;
  }

}
