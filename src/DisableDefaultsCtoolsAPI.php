<?php

/**
 * Representation of ctools API definition.
 */
class DisableDefaultsCtoolsAPI {

  /**
   * @var string
   */
  public $owner;

  /**
   * @var string
   */
  public $api;

  /**
   * @var string[]
   */
  protected $modules;

  /**
   * Constructor.
   *
   * @param string $owner
   * @param string $api
   */
  public function __construct($owner, $api) {
    $this->owner = $owner;
    $this->api = $api;
  }

  /**
   * Magic method to convert object to string.
   */
  public function __toString() {
    return $this->owner . ':' . $this->api;
  }

  /**
   * Creates API object from ctools schema export definition.
   *
   * @param array $export
   *
   * @return \DisableDefaultsCtoolsAPI
   */
  public static function fromExport(array $export) {
    return new self($export['api']['owner'], $export['api']['api']);
  }


  /**
   * Retrieve list of modules that implement this API.
   *
   * @return string[]
   */
  public function getImplementingModules() {
    if (!isset($this->modules)) {
      ctools_include('plugins');
      $hook = ctools_plugin_api_get_hook($this->owner, $this->api);
      $modules = module_implements($hook);
      // Check if each module implements the given api.
      foreach ($modules as $module) {
        $function = $module . '_' . $hook;
        $info = $function($this->owner, $this->api);
        if (!empty($info)) {
          $this->modules[] = $module;
        }
      };
    }
    return $this->modules;
  }



}