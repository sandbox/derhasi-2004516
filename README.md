# Disable defaults

## Features

The module provides a way to disable some hooks for specific modules via the
settings.php. So defaults of views or panels can be removed and can be exported
to a feature with the same machine name.

The module can also be used to disable other hooks, like form_alter.

A general rule for using this module: **Only use it if you are aware of the
 consequences!!**

## Example

The commerce_order_iu module provides three views:

* commerce_orders
* commerce_order_revisions
* commerce_user_orders

If we want to override those views in our own feature, we now can do that with
the following steps:

1. Edit all three views so they will be stored in the database.
2. Add the following code your settings.php

```
$conf['disable_defaults'] = array(
  'views_default_views' => array(
    'commerce_order_ui',
  ),
);
```

3. Clear the cache
4. Export the three (now non-default views) to your feature.
5. Done.
